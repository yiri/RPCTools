package com.qyl;

import org.apache.log4j.PropertyConfigurator;

import com.qyl.framework.client.impl.RPCClient;
import com.qyl.framework.proxy.RpcInvokeProxy;
import com.qyl.framework.rpc.IRpcService;
import com.qyl.service.RpcServiceImpl;

/**
 * 
 * @author created by 齐翌来（qiyilai@foxmail.com）
 * 
 * 2019年6月21日
 * 
 * 启动客户端
 * @description
 *
 */
public class RPCClientApplication {
		
	public static void main(String[] args) {
		PropertyConfigurator.configure(RPCClientApplication.class.getResourceAsStream("/log4j.properties"));;
		RPCClient client = new RPCClient("192.168.1.100", 9501);
		client.startAsync();
		IRpcService service = new RpcServiceImpl();
		RpcInvokeProxy<IRpcService> proxy = new RpcInvokeProxy<IRpcService>(client);
		IRpcService serviceProxy  = proxy.bind(service);
		System.out.println(serviceProxy.echo());
	}

}
