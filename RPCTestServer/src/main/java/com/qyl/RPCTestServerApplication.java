package com.qyl;

import org.apache.log4j.PropertyConfigurator;

import com.qyl.framework.server.impl.RPCServer;

public class RPCTestServerApplication {
	
	public static void main(String[] args) {
		PropertyConfigurator.configure(RPCTestServerApplication.class.getResourceAsStream("/log4j.properties"));;
		RPCServer server = new RPCServer(9501);
		server.startAsync();
	}
}
