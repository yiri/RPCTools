package com.qyl.service;

import com.qyl.framework.rpc.IRpcService;

public class RpcServiceImpl implements IRpcService {

	public String echo() {
		System.out.println("rpc echo executed...");
		return "hello world";
	}

}
