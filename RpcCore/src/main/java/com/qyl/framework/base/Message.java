package com.qyl.framework.base;

import io.netty.buffer.ByteBuf;

public interface Message {
    
	public ByteBuf toBuffer() throws Exception;
	
	public <T extends Message> T fromBuffer(ByteBuf buffer) throws Exception;
	
}
