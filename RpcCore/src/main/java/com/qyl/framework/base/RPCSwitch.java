package com.qyl.framework.base;

import com.google.common.util.concurrent.AbstractService;

/**
 * 
 * @author created by 齐翌来（qiyilai@foxmail.com）
 * 
 * 2019年2月17日
 * 
 * @description
 *  rpc服务类
 */
public abstract class RPCSwitch extends AbstractService {
	
	protected int port;

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
}
