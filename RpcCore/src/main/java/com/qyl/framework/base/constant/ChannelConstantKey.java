package com.qyl.framework.base.constant;

import com.qyl.framework.base.value.BlockingRequestValue;

import io.netty.util.AttributeKey;

/**
 * 
 * @author created by 齐翌来（qiyilai@foxmail.com）
 * 
 * 2019年4月21日
 * 
 * @description netty常量配置
 *
 */
public class ChannelConstantKey {
	
	public static final AttributeKey<BlockingRequestValue> BLOCKING_REQUEST = AttributeKey.newInstance("BLOCKING_REQUEST");
}
