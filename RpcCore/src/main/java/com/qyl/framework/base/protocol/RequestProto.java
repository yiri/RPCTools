package com.qyl.framework.base.protocol;

/**
 * @author created by 齐翌来（qiyilai@foxmail.com）
 * 
 * 2019年2月17日
 * 
 * @description REQUEST请求协议格式
 */
public class RequestProto {
	
	private long requestId;
	
	/**
	 * 类名
	 */
	private String className;
	
	/**
	 * 方法名称
	 */
	private String methodName;
	
	/**
	 * 方法参数类型
	 */
    private Class<?>[] paramClasses;
    
    /**
     * 方法参数值
     */
    private Object[] paramValues;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Class<?>[] getParamClasses() {
		return paramClasses;
	}

	public void setParamClasses(Class<?>[] paramClasses) {
		this.paramClasses = paramClasses;
	}

	public Object[] getParamValues() {
		return paramValues;
	}

	public void setParamValues(Object[] paramValues) {
		this.paramValues = paramValues;
	}

	public RequestProto(long requestId, String className, String methodName, Class<?>[] paramClasses, Object[] paramValues) {
		super();
		this.requestId = requestId;
		this.className = className;
		this.methodName = methodName;
		this.paramClasses = paramClasses;
		this.paramValues = paramValues;
	}
	
	public RequestProto() {

	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
}
