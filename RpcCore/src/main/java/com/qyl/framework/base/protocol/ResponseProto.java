package com.qyl.framework.base.protocol;

/**
 * 
 * @author created by 齐翌来（qiyilai@foxmail.com）
 * 
 * 2019年4月21日
 * 
 * @description RESPONSE 协议格式
 *
 */
public class ResponseProto {
	
	/**
	 * 协议id
	 */
	private long reponseId;
	
	/**
	 * 返回结果对象
	 */
	private Object response;

	public long getReponseId() {
		return reponseId;
	}

	public void setReponseId(long reponseId) {
		this.reponseId = reponseId;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	public ResponseProto(long reponseId, Object response) {
		super();
		this.reponseId = reponseId;
		this.response = response;
	}

	public ResponseProto() {
	}
}
