package com.qyl.framework.base.request;

import com.qyl.framework.base.Message;

import io.netty.buffer.ByteBuf;

public abstract class RpcRequest implements Message {
	
	@Override
	public <T extends Message> T fromBuffer(ByteBuf buffer)throws UnsupportedOperationException {
		throw new UnsupportedOperationException("REQUEST IS NOT SUPPORT");
	}
}
