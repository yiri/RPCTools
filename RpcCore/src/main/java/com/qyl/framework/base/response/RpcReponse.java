package com.qyl.framework.base.response;

import com.qyl.framework.base.Message;

import io.netty.buffer.ByteBuf;

public abstract class RpcReponse implements Message {

	@Override
	public ByteBuf toBuffer() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("RESPONSE IS NOT SUPPORT");
	}
}
