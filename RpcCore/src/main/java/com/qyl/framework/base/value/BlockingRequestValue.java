package com.qyl.framework.base.value;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class BlockingRequestValue implements LazyValue{

	private Map<Long, Object> resultMap = new ConcurrentHashMap<Long, Object>();
	
	private Map<Long, CountDownLatch> latchMap = new ConcurrentHashMap<>();
	
	private static final int DEFAULT_TIME_OUT = 15000;
	
	private int timeOut = DEFAULT_TIME_OUT;

	@Override
	public Object getValue(long key) throws Exception {
		CountDownLatch latch = latchMap.get(key);
		if (latch == null) {
			throw new NullPointerException(String.format("requetId = %s对应的CountDownLatch 缺失", key));
		}
		latch.await(timeOut, TimeUnit.MILLISECONDS);
		return resultMap.get(key);
	}
	
	@Override
	public Object getValue(long key, int timeOut) throws Exception {
		CountDownLatch latch = latchMap.get(key);
		if (latch == null) {
			throw new NullPointerException(String.format("requetId = %s对应的CountDownLatch 缺失", key));
		}
		latch.await(timeOut, TimeUnit.MILLISECONDS);
		latchMap.remove(key);
		return resultMap.remove(key);
	}

	public BlockingRequestValue() {
		
	}
	
	public BlockingRequestValue(int timeOut) {
		this.timeOut = timeOut;
	}
	
	public void request(long key) {
		latchMap.put(key, new CountDownLatch(1));
	}
	
	public void response(long key, Object value){
		CountDownLatch latch = latchMap.get(key);
		if (latch != null && value != null) {
			resultMap.put(key, value);
			latch.countDown();
		}
	}
}
