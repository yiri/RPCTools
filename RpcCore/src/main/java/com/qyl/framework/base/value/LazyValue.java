package com.qyl.framework.base.value;

/**
 * 获取值
 * @author created by 齐翌来（qiyilai@foxmail.com）
 * 
 * 2019年4月20日
 * 
 * @description
 *
 */
public interface LazyValue {

	 public Object getValue(long key) throws Exception;
	 
	 public Object getValue(long key, int timeOut) throws Exception;
}
