package com.qyl.framework.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qyl.framework.base.constant.ChannelConstantKey;
import com.qyl.framework.base.protocol.ResponseProto;
import com.qyl.framework.base.value.BlockingRequestValue;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.Attribute;

public class ResponseHandler extends ChannelInboundHandlerAdapter{
	
	private static Logger logger = LoggerFactory.getLogger(ResponseHandler.class);
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		Attribute<BlockingRequestValue> attr = ctx.channel().attr(ChannelConstantKey.BLOCKING_REQUEST);
		BlockingRequestValue blockRequestValue = attr.get();
		
		if (msg instanceof ResponseProto) {
			ResponseProto proto = ResponseProto.class.cast(msg);
			blockRequestValue.response(proto.getReponseId(), proto.getResponse());
		} else {
			logger.error("返回协议类型错误 msg = {}", msg.toString());
		}
		return;
	}
}
