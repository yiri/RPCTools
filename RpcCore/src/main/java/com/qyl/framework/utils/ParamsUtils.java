package com.qyl.framework.utils;

import com.qyl.framework.base.Message;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * 
 * @author created by 齐翌来（qiyilai@foxmail.com）
 * 
 * 2019年2月17日
 * 
 * @description
 * 参数序列化
 */
public class ParamsUtils {
	
	public static ByteBuf toBuffer(Message param) {
		try {
			return param.toBuffer();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static <T extends Message> T fromBuffer(ByteBuf buffer, Class<T> classz) {
		try {
			T t = classz.newInstance();
			t = t.fromBuffer(buffer);
			return t;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T fromBufferForSimpleType(ByteBuf buffer, Class<T> classz) {
		if (classz == Byte.class || classz == byte.class){
			return (T)new Byte(buffer.readByte());
		} else if (classz == Short.class || classz == short.class){
			return (T)new Short(buffer.readShort());
		} else if (classz == Long.class || classz == long.class){
			return (T)new Long(buffer.readLong());
		} else if (classz == Integer.class || classz == int.class){
			return (T)new Integer(buffer.readInt());
		} else if (classz == Boolean.class || classz == boolean.class){
			return (T)new Boolean(buffer.readBoolean());
		} else if (classz == Float.class || classz == float.class){
			return (T)new Float(buffer.readFloat());
		} else if (classz == Double.class || classz == double.class){
			return (T)new Double(buffer.readDouble());
		} else if (classz == Character.class || classz == char.class){
			return (T)new Character(buffer.readChar());
		} else if (classz == String.class){
			int strLen = buffer.readUnsignedShort();
			byte[] bytes = new byte[strLen];
			buffer.readBytes(bytes);
			return (T)new String(bytes);
		}
		throw new UnsupportedOperationException("fromBufferForSimpleType method only support Byte,Short,Integer,Long,Float,Double,Boolean and String class");
	}
	
	@SuppressWarnings("unchecked")
	public static <T> ByteBuf toBufferForSimpleType(T t){
		Class<T> classz  = (Class<T>)t.getClass();
		ByteBuf buffer = Unpooled.buffer();
		if (classz == Byte.class || classz == byte.class){
			buffer.writeByte((Byte)t);
		} else if (classz == Short.class || classz == short.class){
			buffer.writeShort((Short)t);
		} else if (classz == Long.class || classz == long.class){
			buffer.writeLong((Long)t);
		} else if (classz == Integer.class || classz == int.class){
			buffer.writeInt((Integer)t);
		} else if (classz == Boolean.class || classz == boolean.class){
			buffer.writeBoolean((Boolean)t);
		} else if (classz == Float.class || classz == float.class){
			buffer.writeFloat((Float)t);
		} else if (classz == Double.class || classz == double.class){
			buffer.writeDouble((Double)t);
		} else if (classz == Character.class || classz == char.class){
			buffer.writeChar((Character)t);
		} else if (classz == String.class){
			String src = (String)t;
			byte[] bytes = src.getBytes();
			buffer.writeShort(bytes.length);
			buffer.writeBytes(bytes);
		} else {
			throw new UnsupportedOperationException("fromBufferForSimpleType method only support Byte,Short,Integer,Long,Float,Double,Boolean and String class");
		}
		return buffer;
	}
}
